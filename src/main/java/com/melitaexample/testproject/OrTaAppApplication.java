package com.melitaexample.testproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.logging.Logger;

@SpringBootApplication(exclude = {
        org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class,
        org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration.class}
)
public class OrTaAppApplication {

    private static final Logger LOGGER = Logger.getLogger(OrTaAppApplication.class.getName());

    public static void main(String[] args) {

        SpringApplication.run(OrTaAppApplication.class, args);
        LOGGER.info("OrTaAppApplication STARTED");
    }

}
