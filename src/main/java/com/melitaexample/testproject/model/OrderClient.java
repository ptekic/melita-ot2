package com.melitaexample.testproject.model;

import javax.validation.constraints.NotNull;

public class OrderClient {

    private String status;

    private String customerName;

    private String packName;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getPackName() {
        return packName;
    }

    public void setPackName(String packName) {
        this.packName = packName;
    }

    public boolean valid(OrderClient orderClient){
        boolean valid = false;
        if(orderClient.status != null){
           valid = (orderClient.status.equalsIgnoreCase("ACTIVE")
                   || orderClient.status.equalsIgnoreCase("ACCEPTED"));
        }
        return valid;
    }

    @Override
    public String toString() {
        return "OrderClient{" +
                "status='" + status + '\'' +
                ", customerName='" + customerName + '\'' +
                ", packName='" + packName + '\'' +
                '}';
    }

}
