package com.melitaexample.testproject.api;

import com.melitaexample.testproject.model.OrderClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

@RestController
@RequestMapping("/api/v1/validate")
public class ValidateOrderController {

    private static final Logger LOGGER = Logger.getLogger(ValidateOrderController.class.getName());

    @PostMapping()
    public ResponseEntity<Boolean> validateOrder(@RequestBody OrderClient orderClient) {
        OrderClient oc = new OrderClient();
        LOGGER.info("Validating order: " + orderClient.toString());
        return oc.valid(orderClient) ? new ResponseEntity<>(true, HttpStatus.OK) : new ResponseEntity<>(false,HttpStatus.BAD_REQUEST);
    }

}
